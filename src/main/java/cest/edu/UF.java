package main.java.cest.edu;

public class UF extends Cidade {	
	
	public String Sigla;
	public String Descricao;
	
	public String getSigla() {
		return Sigla;
	}
	public void setSigla(String sigla) {
		this.Sigla = sigla;
	}
	public String getDescricao() {
		return Descricao;
	}
	public void setDescricao(String descricao) {
		this.Descricao = descricao;
	}
	
	public UF(Cidade cidade, String logradouro, Integer numero, String cep) {
		super(cidade, logradouro, numero, cep);
	}
	
}
