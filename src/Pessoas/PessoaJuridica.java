package Pessoas;

import main.java.cest.edu.Endereco;

public class PessoaJuridica implements Pessoa {	
	private String nome;
    private String cnpj;
    private Endereco endereco;
	
    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public PessoaJuridica(String nome, String cnpj, Endereco endereco) {
        this.nome = nome;
        this.cnpj = cnpj;
        this.endereco = endereco;
    }
    @Override
    public String getNome() {
        return this.nome;
    }

    @Override
    public String getEndereco() {
        return this.endereco.toString();
    }


	@Override
	public String getCPF() {
		return this.getCPF();
	}
}
	
