package Atendimento;

import java.math.BigDecimal;
import Pessoas.PessoaFisica;

public class Funcionario extends PessoaFisica {	
	
	public String Cargo;
	public BigDecimal Salario;
	
	public String getCargo() {
		return Cargo;
	}
	public void setCargo(String cargo) {
		this.Cargo = cargo;
	}
	public BigDecimal getSalario() {
		return Salario;
	}
	public void setSalario(BigDecimal salario) {
		this.Salario = salario;
	}
	
	public Funcionario(String nome, String cpf) {
		super(nome, cpf);
		
	}
	


}
